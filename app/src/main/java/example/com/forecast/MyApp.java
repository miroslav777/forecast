package example.com.forecast;


import android.app.Application;

import example.com.forecast.di.component.AppComponent;
import example.com.forecast.di.component.DaggerAppComponent;
import example.com.forecast.di.module.AppModelModule;


public class MyApp  extends Application {
    private static AppComponent appComponent;


    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent
                .builder()
                .appModelModule(new AppModelModule(getApplicationContext()))
                .build();

    }

    public static AppComponent getAppComponent(){
        return appComponent;
    }
}
