package example.com.forecast.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.arellomobile.mvp.MvpFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.com.forecast.R;
import example.com.forecast.adapter.ListCitiesAdapter;
import example.com.forecast.internet.list.city.ListCity;
import example.com.forecast.mvp.presenter.SelectCityPresenter;
import example.com.forecast.mvp.view.SelectCityView;


public class SelectCityFragment extends MvpFragment implements SelectCityView{

    @InjectPresenter
    SelectCityPresenter selectCityPresenter;

    @BindView(R.id.btnSend)
    Button btnSend;

    @BindView(R.id.textViewCityName)
    EditText textViewCityName;

    @BindView(R.id.forecast_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.progress_bar_loading_cities)
    LinearLayout progressBar;

    @BindView(R.id.activity_main_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.select_city_fragment,null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectCityPresenter.searchCity(textViewCityName.getText().toString());
            }
        });

    }

    @Override
    public void showConnectionError() {
        Toast.makeText(getActivity(),"Проверьте интернет соединение",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmptyCityNameError() {
        Toast.makeText(getActivity(),"Вы не указали город",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showListCityNames(ListCity listCity) {

        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        ListCitiesAdapter adapter = new ListCitiesAdapter(listCity.getResults());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                return true;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
                selectCityPresenter.setCity("we");
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setVisibility(View.GONE);
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
    }
    /*
    private void setRecyclerViewItemTouchListener() {

        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(0, 0) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {

            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }*/
}
