package example.com.forecast.activity;

import android.app.Activity;
import android.os.Bundle;
import example.com.forecast.R;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
    }
}
