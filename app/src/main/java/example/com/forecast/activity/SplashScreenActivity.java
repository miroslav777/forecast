package example.com.forecast.activity;

import android.content.Intent;
import android.os.Bundle;

import com.arellomobile.mvp.MvpActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import example.com.forecast.R;
import example.com.forecast.mvp.presenter.SplashPresenter;
import example.com.forecast.mvp.view.SplashView;


public class SplashScreenActivity extends MvpActivity implements SplashView {

    @InjectPresenter
    SplashPresenter splashPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
    }

    @Override
    public void showMainACtivity() {
        launchActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void showSelectCityActivity() {
        launchActivity(new Intent(this, SelectCityActivity.class));
    }

    private void launchActivity(Intent intent){
        startActivity(intent);
        finish();
    }
}
