package example.com.forecast.activity;


import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import example.com.forecast.R;
import example.com.forecast.fragment.SelectCityFragment;

public class SelectCityActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_city_activity);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        SelectCityFragment selectCityFragment = new SelectCityFragment();
        fragmentTransaction.add(R.id.select_city_container,selectCityFragment);
        fragmentTransaction.commit();
    }
}
