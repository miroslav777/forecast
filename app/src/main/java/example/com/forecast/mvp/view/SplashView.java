package example.com.forecast.mvp.view;

import com.arellomobile.mvp.MvpView;

public interface SplashView extends MvpView {
    void showMainACtivity();
    void showSelectCityActivity();
}
