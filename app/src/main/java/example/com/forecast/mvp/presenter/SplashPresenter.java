package example.com.forecast.mvp.presenter;

import android.os.Handler;
import android.text.TextUtils;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import javax.inject.Inject;
import example.com.forecast.MyApp;
import example.com.forecast.mvp.model.City;
import example.com.forecast.mvp.view.SplashView;

@InjectViewState
public class SplashPresenter extends MvpPresenter<SplashView>{


    @Inject
    City city;

    private int secRun = 1000;

    public SplashPresenter(){
       MyApp.getAppComponent().inject(this);
       new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!TextUtils.isEmpty(city.getName())){
                    getViewState().showMainACtivity();
                }
                else {
                    getViewState().showSelectCityActivity();
                }
            }
        },secRun);
    }
}
