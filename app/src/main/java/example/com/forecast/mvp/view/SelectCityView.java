package example.com.forecast.mvp.view;


import com.arellomobile.mvp.MvpView;

import example.com.forecast.internet.list.city.ListCity;

public interface SelectCityView extends MvpView{
    void showConnectionError();
    void showEmptyCityNameError();
    void showListCityNames(ListCity listCity);
    void showProgress();
    void hideProgress();
}
