package example.com.forecast.mvp.presenter;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import example.com.forecast.MyApp;
import example.com.forecast.internet.Repo;
import example.com.forecast.internet.list.city.ListCity;
import example.com.forecast.mvp.model.City;
import example.com.forecast.mvp.view.SelectCityView;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@InjectViewState
public class SelectCityPresenter extends MvpPresenter<SelectCityView> {

    @Inject
    City city;

    @Inject
    Context context;
    private Subscription subscribe;

    public SelectCityPresenter(){
        MyApp.getAppComponent().inject(this);
    }

    public void searchCity(String name){
        if (TextUtils.isEmpty(name)){
            getViewState().showEmptyCityNameError();
            return;
        }
        if (!isOnline()){
            getViewState().showConnectionError();
            return;
        }

        getViewState().showProgress();

        Repo repo = new Repo();

        Observable<ListCity> observable = repo.getListCity(name);

        subscribe = observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ListCity>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showConnectionError();
                    }

                    @Override
                    public void onNext(ListCity listCity) {
                        getViewState().hideProgress();
                        getViewState().showListCityNames(listCity);
                    }
                });
    }

    public void setCity(String name){

    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(subscribe!=null && !subscribe.isUnsubscribed()){
            subscribe.unsubscribe();
        }
    }
}
