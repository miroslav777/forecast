package example.com.forecast.di.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import example.com.forecast.mvp.model.City;

@Module
public class AppModelModule {
    private final Context context;

    public AppModelModule(Context context){
        this.context = context;
    }

    @Singleton
    @Provides
    public City provideCityModel(){
        return new City();
    }

    @Singleton
    @Provides
    public Context provideContext(){
        return context;
    }

}
