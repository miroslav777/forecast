package example.com.forecast.di.component;


import javax.inject.Singleton;
import dagger.Component;
import example.com.forecast.di.module.AppModelModule;
import example.com.forecast.mvp.presenter.SelectCityPresenter;
import example.com.forecast.mvp.presenter.SplashPresenter;

@Singleton
@Component(modules = {AppModelModule.class})
public interface AppComponent {
        void inject(SplashPresenter splashPresenter);
        void inject(SelectCityPresenter selectCityPresenter);
}
