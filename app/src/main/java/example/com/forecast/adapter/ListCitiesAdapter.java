package example.com.forecast.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.com.forecast.R;
import example.com.forecast.internet.list.city.Results;


public class ListCitiesAdapter extends RecyclerView.Adapter<ListCitiesAdapter.ListCitiesHolder>{

    private List<Results> list;

    public ListCitiesAdapter(List<Results> list){
        this.list = list;
    }


    @Override
    public ListCitiesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_from_list_cities,parent,false);
        ListCitiesHolder listCitiesHolder = new ListCitiesHolder(v);
        return listCitiesHolder;
    }

    @Override
    public void onBindViewHolder(ListCitiesHolder holder, int position) {
        holder.city_name_text_view.setText(list.get(position).getFormatted_address());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ListCitiesHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.city_name_text_view)
        TextView city_name_text_view;

        public ListCitiesHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
