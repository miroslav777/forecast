package example.com.forecast.internet.list.city;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListCity {

    @SerializedName("results")
    private List<Results> results;
    @SerializedName("status")
    private String status;

    public List<Results> getResults() {
        return results;
    }

    public void setResults(List<Results> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
