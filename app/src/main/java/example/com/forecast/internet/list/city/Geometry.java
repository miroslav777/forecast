package example.com.forecast.internet.list.city;

import com.google.gson.annotations.SerializedName;

public  class Geometry {
        @SerializedName("bounds")
        private Bounds bounds;
        @SerializedName("location")
        private Location location;
        @SerializedName("location_type")
        private String location_type;
        @SerializedName("viewport")
        private Viewport viewport;

        public Bounds getBounds() {
                return bounds;
        }

        public void setBounds(Bounds bounds) {
                this.bounds = bounds;
        }

        public Location getLocation() {
                return location;
        }

        public void setLocation(Location location) {
                this.location = location;
        }

        public String getLocation_type() {
                return location_type;
        }

        public void setLocation_type(String location_type) {
                this.location_type = location_type;
        }

        public Viewport getViewport() {
                return viewport;
        }

        public void setViewport(Viewport viewport) {
                this.viewport = viewport;
        }
}