package example.com.forecast.internet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import example.com.forecast.internet.list.city.Link;
import example.com.forecast.internet.list.city.ListCity;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.Observable;

public class Repo{

    private final String BASE_URL = "http://maps.googleapis.com/";
    //private final String key = "AIzaSyCRZhkJ87Sjpvn5-RKwqMomImFRSW20AUA";
    private final String[] lang = {"ru"};
    private Gson gson = new GsonBuilder().create();
    private Retrofit retrofit = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .baseUrl(BASE_URL)
            .build();

    private Link link = retrofit.create(Link.class);

    public Observable<ListCity> getListCity (String str){
        return link.getListCity(str,lang[0],false);
    }
}
