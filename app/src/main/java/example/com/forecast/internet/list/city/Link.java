package example.com.forecast.internet.list.city;


import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface Link {

    @GET("maps/api/geocode/json")
    Observable<ListCity> getListCity(@Query("address") String city,
                                     @Query("language") String lang,
                                     @Query("sensor") boolean sensor);
}
